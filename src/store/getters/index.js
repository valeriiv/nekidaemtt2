export const receiveContainers = state => {
  return state.containers;
};

export const getHold = state => {
  return state.hold;
};

export const getProgress = state => {
  return state.progress;
};

export const getReview = state => {
  return state.review;
};

export const getApproved = state => {
  return state.approved;
};
