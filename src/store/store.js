import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import * as actions from "../store/actions/index";
import * as getters from "../store/getters/index";
import * as mutations from "../store/mutation-types/index";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    containers: [
      {
        title: "On hold",
        count: 0
      },
      {
        title: "In progress",
        count: 0
      },
      {
        title: "Needs review",
        count: 0
      },
      {
        title: "Approved",
        count: 0
      }
    ],
    hold: [],
    progress: [],
    review: [],
    approved: []
  },
  actions,
  getters,
  mutations,
  plugins: [createPersistedState()]
});
