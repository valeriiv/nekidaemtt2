export const addCard = (context, payload) => {
  context.commit("ADD_CARD", payload);
};

export const removeCard = (context, payload) => {
  context.commit("REMOVE_CARD", payload);
};

export const shuffleCards = (context, payload) => {
  context.commit("SHUFFLE_CARDS", payload);
};

export const decreaseIndex = (context, payload) => {
  context.commit("DECREASE_INDEX", payload);
};

export const removeDraggableCard = (context, payload) => {
  context.commit("REMOVE_DRAGGABLE_CARD", payload);
};
