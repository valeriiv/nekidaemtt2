export const ADD_CARD = (state, payload) => {
  state.containers[payload.index].count++;

  switch (payload.index) {
    case 0:
      state.hold.splice(state.hold.length, 0, payload.res);
      break;
    case 1:
      state.progress.splice(state.progress.length, 0, payload.res);
      break;
    case 2:
      state.review.splice(state.review.length, 0, payload.res);
      break;
    case 3:
      state.approved.splice(state.approved.length, 0, payload.res);
      break;
  }
};

export const REMOVE_CARD = (state, payload) => {
  if (state.containers.length > 0) {
    state.containers[payload.index].count--;
  }

  if (
    state.hold.length > 0 ||
    state.progress.length > 0 ||
    state.review.length > 0 ||
    state.approved.length > 0
  ) {
    switch (payload.index) {
      case 0:
        state.hold.splice(payload.innerIndex, 1);
        break;
      case 1:
        state.progress.splice(payload.innerIndex, 1);
        break;
      case 2:
        state.review.splice(payload.innerIndex, 1);
        break;
      case 3:
        state.approved.splice(payload.innerIndex, 1);
        break;
    }
  }
};

export const SHUFFLE_CARDS = (state, payload) => {
  switch (payload.position) {
    case 0:
      state.hold = [...payload.newValue];
      break;
    case 1:
      state.progress = [...payload.newValue];
      break;
    case 2:
      state.review = [...payload.newValue];
      break;
    case 3:
      state.approved = [...payload.newValue];
      break;
  }
};

export const DECREASE_INDEX = (state, payload) => {
  if (state.containers.length > 0) {
    state.containers[payload.index].count--;
  }
};

export const REMOVE_DRAGGABLE_CARD = (state, payload) => {
  if (
    state.hold.length > 0 ||
    state.progress.length > 0 ||
    state.review.length > 0 ||
    state.approved.length > 0
  ) {
    switch (payload.outerIndex) {
      case 0:
        state.hold.splice(payload.result, 1);
        break;
      case 1:
        state.progress.splice(payload.result, 1);
        break;
      case 2:
        state.review.splice(payload.result, 1);
        break;
      case 3:
        state.approved.splice(payload.result, 1);
        break;
    }
  }
}
