module.exports = {
    devServer: {
        compress: true,
        hot: true,
        port: 8099,
        open: true
    }
};